const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    getFileContents, 
    RECIPE_DIR,
    WRITER_INPUT_DIR,
    WRITER_OUTPUT_DIR,
    WRITER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('writer-switch instruction type', () => {

    it('inline switch, switch on string with spaces', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "inline-switch.yml"
        const input = "inline-switch.yml";
        const output = "inline-switch.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/inline-switch.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/inline-switch.bin");

        expect(result).toStrictEqual(expected);

    });

    it('inline switch, switch on integer', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "inline-switch-int.yml"
        const input = "inline-switch-int.yml";
        const output = "inline-switch-int.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/inline-switch-int.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/inline-switch-int.bin");

        expect(result).toStrictEqual(expected);

    });

    it('subrecipe reference switch', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "reference-switch.yml"
        const input = "reference-switch.yml";
        const output = "reference-switch.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/reference-switch.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/reference-switch.bin");

        expect(result).toStrictEqual(expected);

    });

})
