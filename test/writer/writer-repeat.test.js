const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    getFileContents, 
    RECIPE_DIR,
    WRITER_INPUT_DIR,
    WRITER_OUTPUT_DIR,
    WRITER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('writer-repeat instruction type', () => {

    it.todo('repeat single add instruction');

    it.todo('repeat single switch instruction');

    it('repeat single add instruction', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "repeat-add.yml"
        const input = "repeat-add.yml";
        const output = "repeat-add.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/repeat-add.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/repeat-add.bin");

        expect(result).toStrictEqual(expected);

    });

})
