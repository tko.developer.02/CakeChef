const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    getFileContents, 
    RECIPE_DIR,
    WRITER_INPUT_DIR,
    WRITER_OUTPUT_DIR,
    WRITER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('writer-add instruction type', () => {

    it('2 byte integer', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "2-byte-int.yml"
        const input = "2-byte-int.yml";
        const output = "2-byte-int.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/2-byte-int.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/2-byte-int.bin");

        expect(result).toStrictEqual(expected);

    });

    it('4 byte integer, little endian', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "4-byte-int-le.yml"
        const input = "4-byte-int-le.yml";
        const output = "4-byte-int-le.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/4-byte-int-le.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/4-byte-int-le.bin");

        expect(result).toStrictEqual(expected);
    })

    it('4 byte integer, big endian', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "4-byte-int-be.yml"
        const input = "4-byte-int-be.yml";
        const output = "4-byte-int-be.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/4-byte-int-be.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/4-byte-int-be.bin");

        expect(result).toStrictEqual(expected);
    })

    it('5 byte integer, little endian, unpadded', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "5-byte-int-le.yml"
        const input = "5-byte-int-le.yml";
        const output = "5-byte-int-le.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/5-byte-int-le.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/5-byte-int-le.bin");

        expect(result).toStrictEqual(expected);
    })

    it('5 byte integer, little endian, padded', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "5-byte-int-le-padded.yml"
        const input = "5-byte-int-le-padded.yml";
        const output = "5-byte-int-le-padded.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/5-byte-int-le-padded.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/5-byte-int-le-padded.bin");

        expect(result).toStrictEqual(expected);
    })

    it('5 byte integer, big endian, unpadded', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "5-byte-int-be.yml"
        const input = "5-byte-int-be.yml";
        const output = "5-byte-int-be.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/5-byte-int-be.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/5-byte-int-be.bin");

        expect(result).toStrictEqual(expected);
    })

    it('5 byte integer, big endian, padded', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "5-byte-int-be-padded.yml"
        const input = "5-byte-int-be-padded.yml";
        const output = "5-byte-int-be-padded.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/5-byte-int-be-padded.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/5-byte-int-be-padded.bin");

        expect(result).toStrictEqual(expected);
    })

    it('4 byte date', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "4-byte-date.yml"
        const input = "4-byte-date.yml";
        const output = "4-byte-date.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/4-byte-date.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/4-byte-date.bin");

        expect(result).toStrictEqual(expected);
    })

    it('4 byte string', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "4-byte-string.yml"
        const input = "4-byte-string.yml";
        const output = "4-byte-string.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/4-byte-string.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/4-byte-string.bin");

        expect(result).toStrictEqual(expected);
    })

    it('5 byte string', () => {

        let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
        const recipe = "5-byte-string.yml"
        const input = "5-byte-string.yml";
        const output = "5-byte-string.bin";

        chef.write(recipe, input, output);

        const result = getFileContents(WRITER_OUTPUT_DIR + "/5-byte-string.bin");
        const expected = getFileContents(WRITER_EXPECTED_DIR + "/5-byte-string.bin");

        expect(result).toStrictEqual(expected);
    })



})
