const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    getFileContents, 
    RECIPE_DIR,
    WRITER_INPUT_DIR,
    WRITER_OUTPUT_DIR,
    WRITER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('writer-pantry instruction type', () => {

    it.todo('stock and print pantry');

    it.todo('stock variable in add instruction');

    it.todo('use stocked variable in add instruction input');

    it.todo('use stocked variable in switch instruction');

    it.todo('use stocked variable in repeat instruction');

    // it('sample', () => {

    //     let chef = new CakeChef(RECIPE_DIR, WRITER_INPUT_DIR, WRITER_OUTPUT_DIR);
    //     const recipe = "inline-switch.yml"
    //     const input = "inline-switch.yml";
    //     const output = "inline-switch.bin";

    //     chef.write(recipe, input, output);

    //     const result = getFileContents(WRITER_OUTPUT_DIR + "/inline-switch.bin");
    //     const expected = getFileContents(WRITER_EXPECTED_DIR + "/inline-switch.bin");

    //     expect(result).toStrictEqual(expected);

    // });

})
