const { 
    initializeDirectories,
 } = require('./utils/test-utils');
const fs = require('fs-extra')

module.exports = async () => {
  initializeDirectories();
  fs.emptyDirSync('./test/writer/output');
  fs.emptyDirSync('./test/reader/output');
};;