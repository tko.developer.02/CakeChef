const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    parseYAML,
    RECIPE_DIR,
    READER_INPUT_DIR,
    READER_OUTPUT_DIR,
    READER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('reader-add instruction type', () => {

    it('2 byte integer', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "2-byte-int.yml"
        const input = "2-byte-int.bin";
        const output = "2-byte-int.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/2-byte-int.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/2-byte-int.yml");

        expect(result).toStrictEqual(expected);
    });

    it('4 byte integer, little endian', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "4-byte-int-le.yml"
        const input = "4-byte-int-le.bin";
        const output = "4-byte-int-le.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/4-byte-int-le.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/4-byte-int-le.yml");

        expect(result).toStrictEqual(expected);
    });

    it('4 byte integer, big endian', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "4-byte-int-be.yml"
        const input = "4-byte-int-be.bin";
        const output = "4-byte-int-be.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/4-byte-int-be.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/4-byte-int-be.yml");

        expect(result).toStrictEqual(expected);
    });

    it('5 byte integer, little endian, unpadded', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "5-byte-int-le.yml"
        const input = "5-byte-int-le.bin";
        const output = "5-byte-int-le.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/5-byte-int-le.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/5-byte-int-le.yml");

        expect(result).toStrictEqual(expected);
    });

    
    it('5 byte integer, little endian, padded', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "5-byte-int-le-padded.yml"
        const input = "5-byte-int-le-padded.bin";
        const output = "5-byte-int-le-padded.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/5-byte-int-le-padded.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/5-byte-int-le-padded.yml");

        expect(result).toStrictEqual(expected);
    });

    it('5 byte integer, big endian, unpadded', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "5-byte-int-be.yml"
        const input = "5-byte-int-be.bin";
        const output = "5-byte-int-be.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/5-byte-int-be.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/5-byte-int-be.yml");

        expect(result).toStrictEqual(expected);
    });

    
    it('5 byte integer, big endian, padded', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "5-byte-int-be-padded.yml"
        const input = "5-byte-int-be-padded.bin";
        const output = "5-byte-int-be-padded.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/5-byte-int-be-padded.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/5-byte-int-be-padded.yml");

        expect(result).toStrictEqual(expected);
    });

    it('4 byte date', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "4-byte-date.yml"
        const input = "4-byte-date.bin";
        const output = "4-byte-date.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/4-byte-date.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/4-byte-date.yml");

        expect(result).toStrictEqual(expected);
    });

    it('4 byte string', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "4-byte-string.yml"
        const input = "4-byte-string.bin";
        const output = "4-byte-string.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/4-byte-string.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/4-byte-string.yml");

        expect(result).toStrictEqual(expected);
    });

    it('5 byte string', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "5-byte-string.yml"
        const input = "5-byte-string.bin";
        const output = "5-byte-string.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/5-byte-string.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/5-byte-string.yml");

        expect(result).toStrictEqual(expected);
    });

})
