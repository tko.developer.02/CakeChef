const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    parseYAML,
    RECIPE_DIR,
    READER_INPUT_DIR,
    READER_OUTPUT_DIR,
    READER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('reader-pantry instruction type', () => {

    it.todo('stock and print pantry');
    
    it.todo('stock variable in add instruction');

    it.todo('use stocked variable in switch instruction');

    it.todo('use stocked variable in repeat instruction');

    // it('sample', () => {

    //     let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
    //     const recipe = "inline-switch.yml"
    //     const input = "inline-switch.bin";
    //     const output = "inline-switch.yml";

    //     chef.read(recipe, input, output);

    //     const result = parseYAML(READER_OUTPUT_DIR + "/inline-switch.yml");
    //     const expected = parseYAML(READER_EXPECTED_DIR + "/inline-switch.yml");

    //     expect(result).toStrictEqual(expected);
    // });
})
