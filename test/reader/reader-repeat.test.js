const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    parseYAML,
    RECIPE_DIR,
    READER_INPUT_DIR,
    READER_OUTPUT_DIR,
    READER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('reader-repeat instruction type', () => {

    it.todo('repeat single switch instruction');

    it('repeat single add instruction', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "repeat-add.yml"
        const input = "repeat-add.bin";
        const output = "repeat-add.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/repeat-add.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/repeat-add.yml");

        expect(result).toStrictEqual(expected);
    });
})
