const { CakeChef } = require('cakechef/src/cakechef') 
const { 
    parseYAML,
    RECIPE_DIR,
    READER_INPUT_DIR,
    READER_OUTPUT_DIR,
    READER_EXPECTED_DIR,
 } = require('../utils/test-utils');

describe('reader-switch instruction type', () => {

    it('inline switch, string with spaces', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "inline-switch.yml"
        const input = "inline-switch.bin";
        const output = "inline-switch.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/inline-switch.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/inline-switch.yml");

        expect(result).toStrictEqual(expected);
    });

    it('inline switch, integer', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "inline-switch-int.yml"
        const input = "inline-switch-int.bin";
        const output = "inline-switch-int.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/inline-switch-int.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/inline-switch-int.yml");

        expect(result).toStrictEqual(expected);
    });

    it('subrecipe reference switch', () => {

        let chef = new CakeChef(RECIPE_DIR, READER_INPUT_DIR, READER_OUTPUT_DIR);
        const recipe = "reference-switch.yml"
        const input = "reference-switch.bin";
        const output = "reference-switch.yml";

        chef.read(recipe, input, output);

        const result = parseYAML(READER_OUTPUT_DIR + "/reference-switch.yml");
        const expected = parseYAML(READER_EXPECTED_DIR + "/reference-switch.yml");

        expect(result).toStrictEqual(expected);
    });

    

})
