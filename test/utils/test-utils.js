const fs = require('fs-extra')
const YAML = require('yaml');

const RECIPE_DIR = './test/recipe';
const WRITER_INPUT_DIR = './test/writer/input';
const WRITER_OUTPUT_DIR = './test/writer/output';
const WRITER_EXPECTED_DIR = './test/writer/expected';
const READER_INPUT_DIR = './test/reader/input';
const READER_OUTPUT_DIR = './test/reader/output';
const READER_EXPECTED_DIR = './test/reader/expected';


function parseYAML(file) { 
    return YAML.parse(fs.readFileSync(file, 'utf8'));
}

function getFileContents(file) {
    return fs.readFileSync(file);
}

function initializeDirectories() {

    const directories = [    
        RECIPE_DIR,
        WRITER_INPUT_DIR,
        WRITER_OUTPUT_DIR,
        WRITER_EXPECTED_DIR,
        READER_INPUT_DIR,
        READER_OUTPUT_DIR,
        READER_EXPECTED_DIR
    ]

    for (const dir in directories) {
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
    }
}

module.exports = {
    getFileContents,
    initializeDirectories,
    parseYAML,
    
    RECIPE_DIR,
    WRITER_INPUT_DIR,
    WRITER_OUTPUT_DIR,
    WRITER_EXPECTED_DIR,
    READER_INPUT_DIR,
    READER_OUTPUT_DIR,
    READER_EXPECTED_DIR,
}