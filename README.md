# CakeChef
[![pipeline status](https://gitlab.com/tko.developer.02/CakeChef/badges/main/pipeline.svg)](https://gitlab.com/tko.developer.02/CakeChef/-/commits/main)
[![coverage report](https://gitlab.com/tko.developer.02/CakeChef/badges/main/coverage.svg)](https://gitlab.com/tko.developer.02/CakeChef/-/commits/main)

A generic, configuration based reader-writer.

## What's in the Pipeline? 

[0.1.1] Conversion to YAML  
[0.1.2] YAML output on consumption of data file  
[0.1.3] Detailed logging  
[0.2.0] The Pantry  
[0.2.1] Reference Notation  
[0.3.0] Sub Recipes  
[0.4.0] Dynamic Recipes   
[0.4.1] CICD   
[0.4.2] Testing  
[0.4.3] Repeat Switch  
[0.4.4] QoL Features  
[0.4.5] Utilities  
[0.9.0] Extensive Documentation  

## Conversion to YAML [v0.1.1]
This update will convert the configuration files into YAML instead of JSON for better ease of use. 

Additionally, recipes will now be required to provide the field type in addition to the field size. This will allow for better parsing for the reader.

## YAML output on consumption of data file [v0.1.2]
This update will produce the corresponding YAML output whenever a reader reads a file. This will allow for readers to generate orders and make it easy to create new orders based on existing data files.

## Logging [v0.1.3]
Logging will be added for easier debugging.

## The Pantry [v0.2.0]

The Pantry is a system for storing variables for later use. With each add step, you can declare specific fields to save in the pantry. Once saved, you can then access those fields by using the reference syntax within the order. 

## Reference Notation [v0.2.1]

Pantry variables can be used within the recipe / order using the reference syntax.

Recipes:  
[type:field]  
repeat:repeat will support pantry references.    
switch:on will support pantry references.  
switch:case will support recipe references.  

Orders:
All field values will support pantry references.



## Sub Recipes [v0.3.0]

Sub recipes are a way for you to extend your recipes across multiple files. 

## Dynamic Recipes [v0.4.0]

This version contains new features for much more dynamic generation of recipes. Specifically,

1) Switch statements  
~~2) Math functions~~ (Moved to v0.4.5)  
3) Date functions

## Repeat Switch [v0.4.3]

This version significantly refactors the internal code structure and processing to better support instruction chaining for more advanced usages.

1) Support for switch instructions in repeat
2) Code refactoring

## QoL Features [v0.4.4]
1) recipe instruction type for referencing subrecipes. 
2) config instruction type for modifying default configurations.
3) skip data type for ignoring data blocks.  

## Utilities [v0.4.5]

This version adds utilities for greater flexibility in inputs.

1) pantry reference support for inputs.  
2) evaluation support (math functions etc.)
3) map support (e.g. if cake_type = 1, map to "Chocolate Cake") 

## Extensive Documentation [v0.9.0] 

Additional documentation before release.

1) Full list of supported date formats (through luxon)
2) Sample use cases (project demos)
3) Developer documentation (project setup etc.)
4) Updated API listing
5) Licensing
6) Contributing guidelines
