const { taste } = require("./chef/reader");
const { bake } = require("./chef/writer");
const { parseYAML } = require("./chef/common");
const { RecipeManager } = require("./chef/recipe");

class CakeChef { 

    constructor(recipeDir, inputDir, outputDir) {
        this.recipeDir = (recipeDir == null) ? './recipe/' : recipeDir + '/';
        this.outputDir = (outputDir == null) ? './output/' : outputDir + '/';
        this.inputDir = (inputDir == null) ? './input/' : inputDir + '/';
    }

    read(recipe, bfile, outfile) {
        const recipeYAML = parseYAML(this.getRecipeFullpath(recipe));
        const bfileFullpath = this.getInputFullpath(bfile);
        const outfileFullpath = this.getOutputFullpath(outfile);
        const recipeManager = new RecipeManager(this.recipeDir);

        taste(recipeYAML, bfileFullpath, outfileFullpath, recipeManager);
    }

    write(recipe, input, bfile) {
        
        const recipeYAML = parseYAML(this.getRecipeFullpath(recipe));
        const inputYAML = parseYAML(this.getInputFullpath(input));
        const bfileFullPath = this.getOutputFullpath(bfile);
        const recipeManager = new RecipeManager(this.recipeDir);

        bake(recipeYAML, inputYAML, bfileFullPath, recipeManager);
    }

    getRecipeFullpath(recipe) {
        return this.recipeDir + recipe;
    }

    getInputFullpath(input) {
        return this.inputDir + input;
    }

    getOutputFullpath(output) {
        return this.outputDir + output;
    }

}

module.exports = { CakeChef };