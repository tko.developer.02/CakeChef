const { parseYAML } = require('./common');

class RecipeManager { 

    constructor(recipeDir) {
        this.recipeDir = recipeDir;
    }

    dereference(reference) { 
        if (this.isRecipeReference(reference)) { 
            const recipeName = reference.replace('$recipe.', '');
            const recipe = parseYAML(this.recipeDir + "/" + recipeName);
            return recipe;
        } else { 
            return reference;
        }
    }

    isRecipeReference(reference) { 
        return (typeof reference === 'string') ? reference.includes('$recipe.') : false;
    }

}


module.exports = { 
    RecipeManager 
}