const fs = require('fs-extra')
const CRC32  = require('crc-32');
const YAML = require('yaml');

function numberToHex(val) {
    return val.toString(16);
}

function asciiToHex(val) {
    let arr = [];
    for (let i=0; i<val.length; i++) {
        const hex = Number(val.charCodeAt(i)).toString(16);
        arr.push(hex);
    }
    return arr.join('')
}

function hexToNumber(hex) { 
    return parseInt(hex, 16);
}

function bufferToNumber(buf) { 
    return hexToNumber(buf.toString('hex'));
}

function hexToAscii(val) {
    return Buffer.from(val, 'hex').toString();
}

function pad(val, len) {
    return val.padStart(len*2, "0");
}

function deleteFileIfExists(file) { 
    if (fs.existsSync(file)) { 
        fs.unlinkSync(file);
    }
}

function deleteAllInDirectory(fileDir) {
    fs.emptyDirSync(fileDir);
}

function calcCRC32(file) {
    return CRC32.buf(fs.readFileSync(file));
}

function calcCRC32OmitLast4Bytes(file) {
    var buffer = fs.readFileSync(file);
    return CRC32.buf(buffer.slice(0, buffer.length-4));
}

function parseYAML(file) { 
    return YAML.parse(fs.readFileSync(file, 'utf8'));
}

function jsonToYAML(json) { 
    return YAML.stringify(json);
}

function writeToFile(file, content) { 
    return fs.writeFileSync(file, content);
}

module.exports = {
    fs,
    pad,
    numberToHex,
    bufferToNumber,
    hexToNumber,
    asciiToHex,
    hexToAscii,
    deleteFileIfExists,
    deleteAllInDirectory,
    calcCRC32,
    calcCRC32OmitLast4Bytes,
    parseYAML,
    jsonToYAML,
    writeToFile
}
