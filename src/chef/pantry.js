
class Pantry { 

    constructor() { 
        this.pantry = {}
    }

    stockAll(list, container) { 
        for (let item in list) { 
            const field = list[item];
            const value = container[field];
    
            this.stock(item, value);
        }
    }
    
    stock(name, val) { 
        this.pantry[name] = val;
    }
    
    get(id) { 
        return this.pantry[id] 
    }
    
    getAll() { 
        return this.pantry;
    }


    clear() {
        this.pantry = {};
    }

    dereference(reference) { 
        if (this.isPantryReference(reference)) { 
            const field = reference.replace('$pantry.', '');
            return this.pantry[field];
        } else { 
            return reference;
        }
        
    }
    
    isPantryReference(reference) { 
        return (typeof reference === 'string') ? reference.includes('$pantry.') : false;
    }

}

module.exports = {
    Pantry
}