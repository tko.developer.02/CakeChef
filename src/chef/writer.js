const { fs, 
        asciiToHex, 
        pad, 
        numberToHex, 
        calcCRC32, 
        deleteFileIfExists 
} = require('./common');
const { logger } = require('./logger');
const { Pantry }  = require('./pantry');

const { DateTime } = require("luxon");

/**
 * Writes the data file according to the steps provided.
 * 
 * @param {*} steps JSON object representing the execution steps for writing the file. 
 * @param {*} input JSON object containing the values to write to the file.
 * @param {*} output The name of the data binary file to write to.
 */
function bake(steps, input, outfile, recipeManager) { 

    logger.info("starting bake");

    deleteFileIfExists(outfile);

    const pantry = new Pantry();

    for (let i=0; i<steps.length; i++) {
        executeStep(steps, steps[i], i, input, outfile, pantry, recipeManager);

    }

    logger.info("completed bake");
}

function executeStep(steps, step, step_index, input, outfile, pantry, recipeManager) {

    const type = step['type'];
    const label = step['label'];
    const body = step['body'];
    
    const nextInput = nextInputStep(input, label);

    switch (type) {
        case 'add': 
            
            const stock = step['stock'];

            logger.info("[execute add] body: " + JSON.stringify(body) + " input: " + JSON.stringify(nextInput) + " stock: " + JSON.stringify(stock));

            add(body, nextInput, outfile, stock, pantry, recipeManager); 
            break;
        case 'repeat': 
            
            const repeat = pantry.dereference(step['repeat']);

            logger.info("[execute repeat] repeat: " + repeat);
            
            repeatStep(steps, body, step_index, repeat, nextInput, outfile, pantry, recipeManager); 
            break;
        case 'add-multiple': 
            break;
        case 'switch': 
            const to = pantry.dereference(step['switch']['on']);
            const switch_body = recipeManager.dereference(step['switch']['case'][to]);

            logger.info("[execute switch] to: " + to);
            steps.splice(step_index+1, 0, ...switch_body);
            logger.info("switched to template: " + JSON.stringify(steps));

            break;
        case 'pantry': 
            console.log(pantry.getAll());
            break;
        case 'crc32': 
            addCRC32(outfile);
            break;
    }
}

function add(body, input, outfile, stock, pantry) {

    for (let field in body) {        
        const type = body[field][0];
        const size = body[field][1];
        const endianness = body[field][2] || 'BE';
        const value = pantry.dereference(input[field]);

        const encoded = encode(type, value, 'hex')
        const padded = pad(encoded, size);

        append(outfile, padded, endianness);
    }

    pantry.stockAll(stock, input);

}

function encode(type, value, encoding) {
    if (encoding == 'hex' && type == 'number') {
        return numberToHex(value);
    } 
    if (encoding == 'hex' && type == 'string') {
        return asciiToHex(value);
    }
    if (encoding == 'hex' && type == 'date') {
        return dateToHex(value);
    }
}

function dateToHex(val) {
    return numberToHex(DateTime.fromISO(val).toUTC().toSeconds());
}

function addCRC32(outfile) {

    const size = 4;
    const value = calcCRC32(outfile);

    const hex = numberToHex(value, size)

    append(outfile, hex);
}


function repeatStep(steps, step, step_index, repeat, input, outfile, pantry, recipeManager) {
    for (let i=0; i<repeat; i++) {
        executeStep(steps, step, step_index, [input[i]], outfile, pantry, recipeManager);
    }
}

function nextInputStep(input, label) {
    const nextInput = input.find( step => step['label'] == label)
    return (nextInput === undefined) ? null : nextInput['body'];
}

function append(outfile, content, endianness) {
    var buf = (endianness == 'LE') ? Buffer.from(content, "hex").reverse() 
                                        : Buffer.from(content, "hex");

    fs.appendFileSync(outfile, buf);

}

module.exports = { bake };