const { 
    fs, 
    deleteFileIfExists, 
    writeToFile,
    jsonToYAML,
    bufferToNumber,
    numberToHex,
    calcCRC32OmitLast4Bytes
} = require('./common');

const { logger } = require('./logger');
const { Pantry }  = require('./pantry');
const { DateTime } = require("luxon");

var jDataView = require('jdataview');
var jd;

/**
 * Reads the data file according to the steps provided.
 * The YAML representation of this data file is saved into the outfile.
 * 
 * @param {*} steps JSON object representing the execution steps for reading the file. 
 * @param {*} file The binary data file to read from.
 * @param {*} outfile The name of the file to write the output to.
 */
function taste(steps, file, outfile, recipeManager) { 

    logger.info("starting taste");

    jd = new jDataView(fs.readFileSync(file));
    
    const output = [];
    const pantry = new Pantry();

    for (let i=0; i<steps.length; i++) {

        if (steps[i]['type'] == 'switch') {
            const step = steps[i];
            const to = pantry.dereference(step['switch']['on']);
            const body = recipeManager.dereference(step['switch']['case'][to]);

            logger.info("[execute switch] to: " + to);

            steps.splice(i, 1, ...body);

            logger.info("switched to template: " + JSON.stringify(steps));
        }
        
        executeStep(steps[i], output, pantry, recipeManager);
        
    }

    deleteFileIfExists(outfile);

    writeToFile(outfile, jsonToYAML(output));

    // console.log("crc32: ", getCRC32(file));

    logger.info("completed taste ", output);
}

function executeStep(step, output, pantry, recipeManager, index) {

    const type = step['type'];
    const label = step['label'];
    const body = step['body'];

    const container = { "label": label };

    if (index != null) {
        container['index'] = index;
    }

    switch (type) {
        case 'add': 
            const stock = step['stock'];

            logger.info("[execute add] body: " + JSON.stringify(body) + " stock: " + JSON.stringify(stock));

            container['body'] = add(label, body, stock, pantry); 
            break;
        case 'repeat': 
            
            const repeat = pantry.dereference(step['repeat']);

            logger.info("[execute repeat] repeat: " + repeat);

            container['body'] = repeatStep(body, repeat, pantry, recipeManager);
            break;
        case 'add-multiple': 
            break;
        case 'pantry':
            console.log(pantry.getAll());
            break;
        case 'switch':
            // const to = pantry.dereference(step['switch']['on']);
            // const switchBody = recipe.dereference(step['switch']['case'][to]);

            // logger.info("[execute switch] to: " + to + " body: " + JSON.stringify(switchBody));

            // executeStep(switchBody, output, pantry, recipe);
            break;
        case 'crc32': 
            console.log("crc32: ", jd.getBytes(4));
            break;
    }

    output.push(container);
}

// function getCRC32(outfile) {

//     const size = 4;
//     const value = calcCRC32OmitLast4Bytes(outfile);

//     console.log("CRC32 val: ", value)

//     const hex = numberToHex(value, size)

//     return hex;
// }


function add(label, body, stock, pantry) {
        
    const container = {};

    for (let field in body) {      
        const type = body[field][0];     
        const size = body[field][1];
        const endianness = body[field][2] || 'BE';

        // console.log("field: ", field);

        if (endianness == 'LE') jd._littleEndian = true;
        if (endianness == 'BE') jd._littleEndian = false;

        switch (type) { 
            case 'signed-number':
                if (size == 1) container[field] = jd.getInt8();
                else if (size == 2) container[field] = jd.getInt16();
                else if (size == 4) container[field] = jd.getInt32();
                else {
                    // incomplete
                }
                break;
            case 'unsigned-number':
            case 'number':
                if (size == 1) container[field] = jd.getUint8();
                else if (size == 2) container[field] = jd.getUint16();
                else if (size == 4) container[field] = jd.getUint32();
                else {
                    const buffer = (endianness == 'BE') ? jd.getBytes(size) : jd.getBytes(size).reverse();
                    container[field] = bufferToNumber(buffer);
                }
                break;
            case 'date':
                container[field] = DateTime.fromSeconds(jd.getInt32()).toUTC();
                break; 
            case 'string':
                container[field] = jd.getString(size);
                break;
            
        }
        // console.log("value: ", container[field]);
    }

    pantry.stockAll(stock, container)

    return container;
}

function repeatStep(step, repeat, pantry, recipeManager) {

    const output = [];

    for (let i=0; i<repeat; i++) {
        executeStep(step, output, pantry, recipeManager, i+1);
    }

    return output;
}

module.exports = { taste };