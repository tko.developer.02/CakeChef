const winston = require('winston');
require('winston-daily-rotate-file');

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
        winston.format.prettyPrint()
    ),
    transports: [
        new winston.transports.Console({level: 'error'}),
        new winston.transports.DailyRotateFile({
            filename: './logs/cc-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '1d'
          })
    ],
    exceptionHandlers: [
        new winston.transports.Console(),
        new winston.transports.DailyRotateFile({
            filename: './logs/cc-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '1d'
          })
      ]
  });

  module.exports = {
      logger
  }